module.exports = function (app, express) {

    const path = require('path');
    const bodyParser = require('body-parser');
    const routes = require('../routes/indexRoutes');
    const favicon = require('serve-favicon');
    const errorHandler = require('./error_handler')(app);

    app.use(favicon(path.join(__dirname, '../public', 'favicon.ico')));
    app.use('/public', express.static(path.join(__dirname, '../public')));

    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        next();
    });


    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json()); // Body parser use JSON data

    app.use('/api/v1/', routes);
    app.use(errorHandler);
}