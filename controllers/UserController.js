/**
 * Created by nataly on 20.11.17.
 */

const userModel = require('../models').User;
const locationModel = require('../models').Location;
const jwt = require('jsonwebtoken');
const config = require('../config/index');
const getPayload = require('./payload');


class UserController {

     async index(req, res, next){
         let users = [];
         users = await userModel.findAll();
         if (users.length >= 0){
             console.log(users.length)
             res.json(users)
         } else next(404)
     }

    async detail(req, res, next){
        const user = await userModel.findById(req.params.nick);
        if (user){
            res.json(user)
        } else {
            next(404);
        }
    }

    async edit(req, res, next){
        const payload = getPayload(req);
        const user = await userModel.findById(req.params.nick);
        user.update(payload).then((result, model) => {
            res.json({
                    message: "success",
                    data: result
            })
        }).catch ((err) => {
            next(err)
        });
    }

    async create(req, res, next){
        const payload = getPayload(req);
        userModel.create(payload).then((result, model) => {
            res.status(201).json({
                    message: 'success',
                    data: result
                });
        }).catch ((err) => {
            next(err)
        });
    }

    async delete(req, res, next){
        const changes = await userModel.destroy({
            where: {
                id: req.params.nick
            }
        });
        if(changes > 0) {
            res.status(204).json({
                    message: 'success'
            });
        } else {
            next(500)
        }
    }

    async indexLocation(req, res, next){
        const user = await userModel.findById(req.params.nick, {
            include: [{
                model: locationModel,
                as: 'Locations',
            }],
        });
        if (user){
            res.json(user)
        } else next(404)

    }

    async detailLocation(req, res, next){
        const location = await locationModel.find({
                where: {
                    UserId: req.params.nick,
                    id: req.params.id
                }
            }
        );

        if (location){
            res.json(location)
        } else next(404)
    }

    async authLogin(req, res, next) {
        const user = await userModel.findOne({
            where: {
                name: req.body.name
            }
        });
        if (!user) {
            next(404)
        } else {
            user.checkPassword(req.body.password, user.password, (err, isMatch) => {
                if (err) {
                    next(401)
                } else {
                    const token = jwt.sign({
                            occupation: user.occupation
                        },
                        config.get('secret'), {
                            expiresIn: '24h'
                        });
                    res.json({
                        message: 'success',
                        token: token,
                        userId: user.id
                    });
                }
            })
        }
    }

    async authRegister(req, res, next) {
        const payload = getPayload(req);
        userModel.create(payload).then((result, model) => {
            const token = jwt.sign({
                    occupation: result.occupation
                },
                config.get('secret'), {
                    expiresIn: '24h'
                });
            res.json({
                message: 'success',
                token: token,
                userId: result.id
            });

        }).catch ((err) => {
            next(err)
        });
    }

}

module.exports = new UserController();