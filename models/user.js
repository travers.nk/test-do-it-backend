'use strict';
const bCrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {

    const saltRounds = 10;

    const classMethods = {
        associate: models => {
            models.User.hasMany(models.Location);
        }
    };

    const instanceMethods = {
        checkPassword: (passwordToCheck, hashPassword, cb) => {
            bCrypt.compare(passwordToCheck, hashPassword, (err, isMatch) => {
                if (err) return cb(err);
                cb(null, isMatch);
            });
        }
    };

    const model = {
        name: {
            type: DataTypes.STRING,
            unique: true,
            validate: {
                len: {
                    args: 5,
                    msg: "Name must be atleast 5 characters in length"
                },
                isAlphanumeric: true,
                notEmpty: true,
            },
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true,
                len: {
                    args: 3,
                    msg: "Password must be atleast 3 characters in length"
                }
            },
            allowNull: false,
        },
        age: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true,
                min: 1,
                max: 99,
            }
        },
        occupation: {
            type: DataTypes.STRING,
            validate: {
                isAlpha: true
            }
        }
    };

    const User = sequelize.define('User', model, {classMethods, instanceMethods});

    User.beforeCreate((user) => {
        return bCrypt.hash(user.password, saltRounds).then(hash => user.password = hash);
    });

    return User;
};
