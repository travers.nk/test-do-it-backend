/**
 * Created by nataly on 20.11.17.
 */
const userRoutes = require('./userRoutes');
const authRoutes = require('./authRoutes');
const locationRoutes = require('./locationRoutes');
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const config = require('../config/index');

router.use('/auth', authRoutes);

router.use( (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-requested-with'];
    if (req.method !== "OPTIONS"){
        if (token) {
            jwt.verify(token, config.get('secret'), (err, decoded) => {
                if (err) {
                    next(401)
                } else {
                    req.decoded = decoded;
                    next();
                }
            });

        } else {
            return next(403)
        }
    } else next();
});

router.use('/', userRoutes);
router.use('/location', locationRoutes);

module.exports = router;
